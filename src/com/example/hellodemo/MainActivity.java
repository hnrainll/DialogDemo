package com.example.hellodemo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public void showCustomDialog(View v) {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final View view = getLayoutInflater().inflate(R.layout.dialog_signin, null);
		builder.setView(view);
		builder.setPositiveButton("Success",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						
						EditText et = (EditText) view.findViewById(R.id.username);
						String text = et.getText().toString();						
						Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
					}
				}).setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

					}
				});
		builder.create().show();
	}

	public void showFragmentDialog(View v) {

		FragmentManager fm = getSupportFragmentManager();
		MyDialogFragment mdf = new MyDialogFragment();
		mdf.show(fm, "dialog");
	}

	public void showAlertDialog(View v) {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("关于");
		builder.setMessage(getAboutText());
		builder.setPositiveButton("ok", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub

			}
		});

		builder.create().show();
	}

	public void showDialog(View v) {

		Dialog dialog = new Dialog(this);
		dialog.setTitle("About");
		dialog.show();
	}

	/**
	 * 
	 * <string name="about_text">E-cast for Android\n \n版本 : v1.0.1 \n \n公司 :
	 * 深圳市易智泰科技有限公司\n \n电话 : 13352986006 \n\t\t\t : 13510752383\n \n邮件 :
	 * mega666l@163.com \n\t\t\t : 0400220126@163.com
	 * 
	 * @return
	 */
	private String getAboutText() {
		Resources resources = getResources();
		String appName = resources.getString(R.string.about_app_name);
		String versionName = resources.getString(R.string.about_version);
		String company = resources.getString(R.string.about_company);
		String phone1 = resources.getString(R.string.about_phone_1);
		String phone2 = resources.getString(R.string.about_phone_2);
		String mail1 = resources.getString(R.string.about_mail_1);
		String mail2 = resources.getString(R.string.about_main_2);

		StringBuilder sb = new StringBuilder();
		sb.append(appName);
		sb.append(versionName + " " + getPackageVersionNumber());
		sb.append(company);
		sb.append(phone1);
		sb.append("\n\t\t  : " + phone2);
		sb.append(mail1);
		sb.append("\n\t\t  : " + mail2);

		return sb.toString();
	}

	private String getPackageVersionNumber() {
		String version = "";
		PackageManager pm = getPackageManager();
		try {

			PackageInfo info = pm.getPackageInfo(getPackageName(), 0);
			version = info.versionName;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return version;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
